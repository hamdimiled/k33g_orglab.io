import {Style, Template, Fragment, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <div class="container">
    <h1 class="title">
      <!-- -->
    </h1>
    <table class="table is-striped is-hoverable" style="width:100%">
      <thead>
        <tr><th>Title</th><th>Date (updated)</th><th>Labels</th></tr>
      </thead>
      <tbody>
      </tbody>
    </table>
  </div>
`

export class IssuesList extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }

  formatDate(dateString) {
    const options = { year: "numeric", month: "long", day: "numeric" }
    return new Date(dateString).toLocaleDateString(undefined, options)
  }

  refreshList(issues) {

    this.$("tbody").innerHTML =
      issues.map(issue => {
        return Fragment.html`
          <tr>
            <td><a href="${issue.web_url}">${issue.title}</a></td>
            <td>${this.formatDate(issue.updated_at)}</td>
            <td>${issue.labels.map(label => Fragment.html`<span class="tag is-link">${label}</span>`).join(" ")}</td>
          </tr>
        `
      }).join("")

  }

  connectedCallback() {
    this.$class("title").innerHTML = this.getAttribute("title")
  }

}

customElements.define('issues-list', IssuesList)
