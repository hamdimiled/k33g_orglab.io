import {Style, Template, Ossicle} from '../js/Ossicle.js'

const sheetTitles = Style.css`
  h1 { color: orange; }
  h2 { color: green; }
`

const sheetParagraph = Style.css`
  p { color: blue; }
`

const template = Template.html`
  <div>
    <h1>Single Page Application</h1>
    <h2>Hello World</h2>
    <p>
      It's a small world la la
    </p>
  </div>
`

class SinglePageApplication extends Ossicle {

  constructor() {
    super()
    this.setup(template.clone(), [sheetTitles, sheetParagraph])
  }
}

customElements.define('single-page-application', SinglePageApplication)

/*
https://developers.google.com/web/updates/2019/02/constructable-stylesheets

Note: As of October 2019, Constructable Stylesheets are only available in Chrome (versions 73 and higher). Support in other browsers can be tracked here.
https://chromestatus.com/feature/5394843094220800
*/

