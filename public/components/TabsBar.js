import {Style, Template, Ossicle} from '../js/Ossicle.js'

const template = Template.html`
  <section class="container">
    <!-- Tabs -->
    <div class="tabs">
      <ul>
        <li id="last" class="tab is-active">
          <a href="#/last">Last 12 news</a>
        </li>

        <li id="posts" class="tab">
          <a href="#/posts">Posts</a>
        </li>

        <li id="conversations" class="tab">
          <a href="#/conversations">Conversations</a>
        </li>

      </ul>
    </div>
    <!-- End of Tabs -->
  </section>
`

class TabsBar extends Ossicle {

  activate(id) {
    this.$classes("tab").forEach(tab => tab.className = "tab")
    this.$id(id).className += " is-active"
  }

  constructor() {
    super()
    this.setup(template.clone(), [Style.sheets.bulma])
  }
}

customElements.define('tabs-bar', TabsBar)
