# k33g_org.gitlab.io

If you want a domain name like [https://k33g_org.gitlab.io/](https://k33g_org.gitlab.io/):

- create a group
- create a project into the group with a name constructed like that: `group_name.gitlab.io`

## CI

```
pages:
  script: 
    - echo "👋🌍"
  artifacts:
    paths:
      - public
  rules:
    - if: $CI_COMMIT_BRANCH == "master"
```

## Preview

> - 🚧 WIP
